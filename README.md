# get it now
`wget https://raw.githubusercontent.com/poppafuze/bashrc/master/.bashrc`  

# bashrc
This is my currently favored .bashrc.
It's a single dotfile.

## Usage
place the dotfile in your home dir

## more info
this works with user-level and root-level .bashrc 

## TODO
- live append is busted - needs history -a in prompt
- study spacemacs-style git clone deployment strategy
- deployment strategy that does not overly interfere with existing rc and profile
- deployment strategy that does not interfere with non-interactive sessions
- could use a deploy script that strips goodies from the old ~/.bashrc

## refs
http://boredzo.org/blog/archives/2016-08-15/colorized-man-pages-understood-and-customized
